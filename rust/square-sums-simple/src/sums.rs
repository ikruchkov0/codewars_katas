use crate::path::Path;
use crate::tree::{build_tree, Tree, NodeSP};

pub fn is_fair_square(n: u32) -> bool {
    let sqrt = (n as f64).sqrt();
    return sqrt-sqrt.floor() == 0_f64;
}

fn dfs(n: u32, node: NodeSP, path: &mut Path) {
	let nd = node.borrow();
	let pairs = nd.pairs();
	let mut sorted_pairs = Vec::new();
	for p in pairs.iter() {
		sorted_pairs.push(p.clone());
	}

	sorted_pairs.sort_by(|a, b| a.borrow().pairs_not_in_path_count(path).cmp(&b.borrow().pairs_not_in_path_count(path)));

	for p in sorted_pairs {
		let v = p.borrow().value();

		if path.contains(v) {
			continue;
		}

		path.push(v);

		if path.count() == n {
			return;
		}

		dfs(n, p, path);
		if path.count() == n {
			return;
		}

		path.pop();
	}
}

fn square_sums_for_tree(n: u32, tree: &Tree) -> Option<Vec<u32>> {
	for root in tree.roots().iter() {
		let mut path = Path::new();
		let node = root.borrow();
		path.push(node.value());
		dfs(n, root.clone(), &mut path);
		if path.count() == n {
			return Some(path.to_array());
		}
	}
	return None;
}

pub fn square_sums(n: u32) -> Option<Vec<u32>> {
	let tree = build_tree(n);

	return match tree {
		Some(t) => square_sums_for_tree(n, &t),
		None => None,
	};
}