use std::cmp::Ordering;
use std::rc::Rc;
use std::cell::RefCell;

use crate::path::Path;
use crate::sums::is_fair_square;

#[derive(Debug)]
#[derive(Clone)]
pub struct Node {
	value: u32,
	nodes: Vec<NodeSP>,
}

pub type NodeSP = Rc<RefCell<Node>>;

impl Node {
    fn new(n: u32) -> Node {
        return Node{
            value: n,
            nodes: Vec::new(),
        };
    }

    fn cmp(a: &NodeSP, b: &NodeSP) -> Ordering {
        let node_pairs_count = b.borrow().pairs_count();
        return a.borrow().pairs_count().cmp(&node_pairs_count);
    }

    fn add(&mut self, node: &NodeSP) {
        self.nodes.push(node.clone());
    }

    fn pairs_count(&self) -> usize {
        return self.nodes.len();
    }

    pub fn pairs(&self) -> &[NodeSP] {
        return self.nodes.as_slice();
    }

    pub fn value(&self) -> u32 {
        return self.value;
    }

    pub fn pairs_not_in_path_count(&self, path: &Path) -> u32 {
        let mut counter = 0_u32;
        for p in self.nodes.iter() {
            if !path.contains(p.borrow().value) {
                counter += 1;
            }
        }
        return counter;
    }

    fn sort_pairs(&mut self) {
        self.nodes.sort_by(|a, b| Node::cmp(b, a));
    }
}

pub struct Tree {
	n:     u32,
	nodes: Vec<Option<NodeSP>>,
    roots: Vec<NodeSP>,
}

impl Tree {
    fn new(n: u32) -> Tree {
        let mut nodes = Vec::new();
        nodes.resize(n as usize, None);
        return Tree{
            n:     n,
            nodes,
            roots: Vec::new(),
        };
    }

    pub fn roots(&self) -> &[NodeSP] {
        return self.roots.as_slice();
    }

    fn get_or_create(&mut self, n: u32) -> NodeSP {
        let index = (n as usize) - 1;
        return match &self.nodes[index] {
            Some(node) => node.clone(),
            None => {
                let node = Node::new(n);
                let rfc = RefCell::new(node);
                let rc = Rc::new(rfc);
                self.nodes[index] = Some(rc.clone());
                return rc;
            },
        };
    }

    fn add_pair(&mut self, head: u32, tail: u32) {
        let head_node_sp = self.get_or_create(head);
        let tail_node_sp = self.get_or_create(tail);
        head_node_sp.borrow_mut().add(&tail_node_sp);
    }

    fn verify_and_finalize(&mut self) -> bool {
        for n in self.nodes.iter() {
            if let Some(node) = n {
                if node.borrow().pairs_count() == 0 {
                    return false;
                }
                self.roots.push(node.clone());
            } else {
                return false;
            }
        }
        self.sort_pairs();
        return true;
    }

    fn sort_pairs(&mut self) {
        for n in self.roots.iter_mut() {
            n.borrow_mut().sort_pairs();
        }
        self.roots.sort_by(|a, b| Node::cmp(b, a));
    }

}

pub fn build_tree(n: u32) -> Option<Tree> {
	let mut tree = Tree::new(n);

	for i in 1..(n+1) {

		for j in 1..(n+1) {
			if i == j {
				continue
			}

			let sum = i + j;
			if !is_fair_square(sum) {
				continue
			}
			tree.add_pair(i, j)
		}
	}

	if !tree.verify_and_finalize() {
		return None
	}

	return Some(tree)
}

