mod tests;
mod tree;
mod path;
mod sums;

use crate::sums::square_sums;

fn main() {
	for n in 65..2000 {
		let res = square_sums(n);
		match res {
			Some(r) => println!("Result {}", n),
			None => print!("None"),
		};
	}
}
