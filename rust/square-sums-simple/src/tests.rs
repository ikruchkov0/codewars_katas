#[cfg(test)]
mod tests {

    use crate::sums::is_fair_square;

    #[test]
    fn is_fair_square_test() {
        assert_eq!(is_fair_square(16), true);
        assert_eq!(is_fair_square(42), false);
        assert_eq!(is_fair_square(49), true);
    }

    use crate::tree::build_tree;

    #[test]
    fn build_tree_test() -> Result<(), String> {
        let tree = build_tree(15);
        if let Some(t) = tree {
            assert_eq!(t.roots().len(), 15);
        } else {
            return Err(String::from("Unable to build tree"));
        }
        return Ok(());
    }

    use crate::path::Path;

    #[test]
    fn path_test() {
        let mut path = Path::new();
        path.push(2);
        path.push(3);
        path.push(4);
        path.push(5);

        assert_eq!(path.count(), 4);

        path.pop();

        assert_eq!(path.count(), 3);

        let res = path.to_array();

        assert_eq!(res.len(), 3);

        assert_eq!(res[0], 2);
        assert_eq!(res[1], 3);
        assert_eq!(res[2], 4);
    }
}