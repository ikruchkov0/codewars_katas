use std::rc::Rc;
use std::collections::HashMap;

struct PathNode {
	prev:  PathNodeSP,
	value: u32,
}

type PathNodeSP = Option<Rc<PathNode>>;

impl PathNode {
    fn new(n: u32, prev: PathNodeSP) -> Rc<PathNode> {
        return Rc::new(PathNode{
            value: n,
            prev,
        });
    }
}

pub struct Path {
	last:     PathNodeSP,
	count:    u32,
	attached: HashMap<u32, bool>,
}

impl Path {
    pub fn new() -> Path {
        return Path{
            count: 0,
            last: None,
            attached: HashMap::new(),
        };
    }

    pub fn contains(&self, n: u32) -> bool {
        return match self.attached.get(&n) {
            Some(v) => *v,
            None => false,
        };
    }

    pub fn count(&self) -> u32 {
        return self.count;
    }

    pub fn push(&mut self, n: u32) {
        let is_attached = self.attached.get(&n);
        if let Some(_) = is_attached {
            panic!("already attached {}", n);
        }

        let prev = match &self.last {
            Some(node) => Some(node.clone()),
            None => None,
        };
        self.last = Some(PathNode::new(n, prev));

        self.attached.insert(n, true);
        self.count += 1;
    }

    pub fn pop(&mut self) {
        if let Some(last) = &self.last {
            self.attached.remove(&last.value);
            let prev = match &last.prev {
                Some(node) => Some(node.clone()),
                None => None,
            };
            self.last = prev;
            self.count -= 1;
        } else {
            return;
        }
    }

    pub fn to_array(&self) -> Vec<u32> {
        let mut result = Vec::new();
        let mut node = &self.last;
        while let Some(n) = node {
            result.push(n.value);
            node = &n.prev;
        }
        result.reverse();
        return result;
    }
}
