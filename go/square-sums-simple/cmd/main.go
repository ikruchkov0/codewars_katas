package main

import (
	"fmt"

	sums "gitlab.com/ikruchkov0/codewars_katas/go/square-sums-simple/pkg"
)

func main() {
	n := 16
	a := sums.SquareSumsRow(n)
	if a == nil {
		fmt.Printf("No")
	} else {
		fmt.Printf("%+v", a)
	}
}
