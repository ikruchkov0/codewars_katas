package sums

import (
	"fmt"
	"math"
	"sort"
)

func IsFairSquare(n int) bool {
	sqrt := math.Sqrt(float64(n))
	return sqrt-math.Floor(sqrt) == 0
}

func buildTree(n int) *Tree {
	tree := NewTree(n)

	for i := 1; i <= n; i++ {

		for j := 1; j <= n; j++ {
			if i == j {
				continue
			}

			sum := i + j
			if !IsFairSquare(sum) {
				continue
			}
			tree.AddPair(i, j)
		}
	}

	if !tree.VerifyAllNodesHavePairs() {
		return nil
	}

	tree.SortPairs()
	return tree
}

var dfsCount int
var dfsMap = map[uint32]bool{}

func dfs(n int, node *Node, path *Path) {
	dfsCount++

	pairs := make([]*Node, node.PairsCount())
	copy(pairs, node.Pairs())
	sortFn := func(i, j int) bool {
		return pairs[i].PairsNotInPathCount(path) < pairs[j].PairsNotInPathCount(path)
	}
	sort.Slice(pairs, sortFn)

	for _, p := range pairs {
		v := p.Value()

		if path.Contains(v) {
			continue
		}

		path.Push(v)

		if path.Count() == n {
			return
		}

		fmt.Println(path.String())

		dfs(n, p, path)
		if path.Count() == n {
			return
		}

		path.Pop()

		fmt.Println(path.String())
	}
}

func SquareSumsRow(n int) []int {
	tree := buildTree(n)
	if tree == nil {
		return nil
	}

	fmt.Printf("%s\n", tree.String())

	for _, root := range tree.Roots() {
		path := NewPath()
		path.Push(root.Value())
		dfs(n, root, path)
		if path.Count() == n {
			fmt.Println(dfsCount)
			return path.Array()
		}
	}
	return nil
}
