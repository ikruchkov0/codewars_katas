#include <algorithm>
#include <cstdio>
#include <vector>
#include <math.h>
#include <iostream>

class Node {
    private:
        int value;
        std::vector<Node*> pairs;

    public:
        Node(int val) {
            value = val;
            pairs = std::vector<Node*>();
        }

        ~Node() {
            pairs.clear();
        }

        int Value() {
            return value;
        }

        std::vector<Node*> Pairs() {
            return pairs;
        }

        void SortPairs() {
            std::sort(pairs.begin(), pairs.end(), Node::CompareNodes);
        }

        void Add(Node* node ) {
            pairs.push_back(node);
        }

        int PairsCount() {
            return pairs.size();
        }

        static bool CompareNodes(Node *i, Node *j) 
        { 
            return i->PairsCount() < j->PairsCount();
        }
};

class Tree {
    private:
        std::vector<Node*> nodes;

        Node* getOrCreate(int n) {
            Node *node = nodes[n-1];
            if (node == NULL) {
                node = new Node(n);
                nodes[n-1] = node;
            }
            return node;
        }

    public:
        Tree(int n) {
            nodes = std::vector<Node*>(n);
        }

        ~Tree() {
            for (Node *n : nodes) {
                delete n;
            }
            nodes.clear();
        }

        std::vector<Node*> Roots() {
            return nodes;
        }

        void AddPair(int head, int tail) {
            Node *headNode = getOrCreate(head);
	        Node *tailNode = getOrCreate(tail);
            headNode->Add(tailNode);
        }

        bool VerifyAllNodesHavePairs() {
            for (Node *n : nodes) {
                if (n == NULL) {
                    return false;
                }

                if (n->PairsCount() == 0) {
                    return false;
                }
            }

            return true;
        }

        void SortPairs() {
            for (Node *n : nodes) {
                n->SortPairs();
            }
            std::sort(nodes.begin(), nodes.end(), Node::CompareNodes);
        }

};

class PathNode {
    private:
        PathNode *prev;
        int value;
    
    public:
        PathNode(int n, PathNode *prevNode) {
            value = n;
            prev = prevNode;
        }

        int Value() {
            return value;
        }

        PathNode* Prev() {
            return prev;
        }
};

class Path {
    private:
        PathNode *first;   
        PathNode *last;
        int count;
        std::vector<bool> attached;

        std::vector<int> toVector(PathNode *node, std::vector<int> v) {
            PathNode *prev = node->Prev();
            while (node != NULL) {
                v.push_back(node->Value());
                node = node->Prev();
            }
            return v;
        }

    public:
        Path(int capacity) {
            count = 0;
            last = NULL;
            attached = std::vector<bool>(capacity);
        }

        ~Path() {
            PathNode *node = last;
            while (node != NULL){
                PathNode *prev = node->Prev();
                delete node;
                node = prev;
            }
            attached.clear();
        }

        bool Contains(int n) {
            return attached[n];
        }

        int Count() {
            return count;
        }

        void Push(int n) {
            if (attached[n]) {
                throw "Already attached";
            }

            PathNode *prev = last;
            last = new PathNode(n, prev);

            if (prev == NULL) {
                first = last;
            }

            attached[n] = true;
            count++;
        }

        void Pop() {
            if (last == NULL) {
                return;
            }
            attached[last->Value()] = false;
            PathNode *prev = last->Prev();
            delete last;
            last = prev;
            count--;
        }

        std::vector<int> ToVector() {
            return toVector(last, std::vector<int>());
        }


};

struct NodesComparer {
    private:
        Path *path;

        int pairsNotInPath(Node* node) {
            int count = 0;
            for (Node *n : node->Pairs()) {
                if (path->Contains(n->Value())){
                    continue;
                }
                count++;
            }
            return count;
        }

    public:

        NodesComparer(Path *p) {
            path = p;
        }

        bool operator() (Node* i, Node* j) {
            return pairsNotInPath(i) < pairsNotInPath(j);
        }
};

bool is_fair_square(int n) {
	double sqrtVal = sqrt((double)n);
	return sqrtVal - floor(sqrtVal) == 0;
}

Tree* build_tree(int n) {
	Tree *tree = new Tree(n);

	for (int i = 1; i <= n; i++) {

		for (int j = 1; j <= n; j++) {
			if (i == j) {
				continue;
			}

			int sum = i + j;
			if (!is_fair_square(sum)) {
				continue;
			}

			tree->AddPair(i, j);
		}
	}

	if (!tree->VerifyAllNodesHavePairs()) {
    delete tree;
		return NULL;
	}

	tree->SortPairs();
	return tree;
}

void dfs(int n, Node *node, Path* path) {
    std::vector<Node*> pairs(node->PairsCount());
    std::vector<Node*> src = node->Pairs();
    std::copy(src.begin(), src.end(), pairs.begin());

    std::sort(pairs.begin(), pairs.end(), NodesComparer(path) );

	for (Node *p : pairs) {
		int v = p->Value();

		if (path->Contains(v)) {
			continue;
		}

		path->Push(v);

		if (path->Count() == n) {
			return;
		}

		dfs(n, p, path);
		if (path->Count() == n) {
			return;
		}

		path->Pop();
	}

    pairs.clear();
}

std::vector<int> square_sums_row(int n) {
    Tree *tree = build_tree(n);
	if (tree == NULL) {
		return std::vector<int>();
	}

    for (Node *root : tree->Roots()) {
        Path *path = new Path(n);
		path->Push(root->Value());
		dfs(n, root, path);
		if (path->Count() == n) {
            std::vector<int> result = path->ToVector();
            delete path;
			return result;
		}
        delete path;
	}
    delete tree;
	return std::vector<int>();
}

int main() {
    for (int n = 2 ; n <= 2000 ; n++) {
        std::vector<int> res = square_sums_row(n);
        std::cout << n << ": " << res.size() << "\n";
    }
}